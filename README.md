# Hands-on exercise interactive data visualization in Python

(c) 2015, Jan Aerts, Visual Data Analysis Lab, KU Leuven, http://vda-lab.be

In this tutorial, we will use python Bokeh (http://bokeh.pydata.org) to generate visualizations based on a flights dataset. This tutorial holds numerous code snippets that can by copy/pasted and modified for your own purpose. The contents of this tutorial is available under the CC-BY license.

This tutorial is in the form of an ipython notebook, which means you can run it interactively. For ipython installation instructions, see http://ipython.org/